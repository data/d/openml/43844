# OpenML dataset: Coronavirus-Worldwide-Dataset

https://www.openml.org/d/43844

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
From World Health Organization - On 31 December 2019, WHO was alerted to several cases of pneumonia in Wuhan City, Hubei Province of China. The virus did not match any other known virus. This raised concern because when a virus is new, we do not know how it affects people.
So daily level information on the affected people can give some interesting insights when it is made available to the broader data science community.
The European CDC publishes daily statistics on the COVID-19 pandemic. Not just for Europe, but for the entire world. We rely on the ECDC as they collect and harmonize data from around the world which allows us to compare what is happening in different countries.
Content
This dataset has daily level information on the number of affected cases, deaths and recovery etc. from coronavirus. It also contains various other parameters like average life expectancy, population density, smocking population etc. which users can find useful in further prediction that they need to make.
The data is available from 31 Dec,2019.
Inspiration
Give people weekly data so that they can use it to make accurate predictions.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43844) of an [OpenML dataset](https://www.openml.org/d/43844). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43844/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43844/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43844/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

